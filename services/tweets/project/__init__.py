import os 
from flask import Flask, jsonify

# instanciamos la app
app = Flask(__name__)

@app.route('/tweets', methods=['GET'])
def get_tweets():
    basedir = os.path.abspath(os.path.dirname(__file__))
    tweets_path = os.path.join(basedir, './tweets.txt')
    with open(tweets_path) as f:
        file_content = f.read()
    
    return file_content 

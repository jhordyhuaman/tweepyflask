import tweepy

from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
from config import CONSUMER_KEY, CONSUMER_SECRET, \
                   ACCESS_TOKEN, ACCESS_TOKEN_SECRET

class TwitterStreamer():
    def __init__(self):
        pass

    def stream_tweets(self, fetched_tweets_filename, hash_tag_list):
        listener = StdOutListener(fetched_tweets_filename)
        auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
        stream = Stream(auth, listener)
        stream.filter(track=hash_tag_list)


class StdOutListener(StreamListener):
    def __init__(self, fetched_tweets_filename):
       self.fetched_tweets_filename = fetched_tweets_filename
    
    def on_data(self, data):
        try:
            print(data)
            with open(self.fetched_tweets_filename, 'a') as tf:
                tf.write(data)
            return True
        except BaseException as e:
            print("error on_data %s" %str(e))
        return True
    def on_error(self, status):
        print(status)

if __name__ == '__main__':
    hash_tag_list = ["@Huawei"]
    fetched_tweets_filename = "tweets.txt"
    twitter_stream = TwitterStreamer()
    twitter_stream.stream_tweets(fetched_tweets_filename, hash_tag_list)

import tweepy

from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
from config import CONSUMER_KEY, CONSUMER_SECRET, \
                   ACCESS_TOKEN, ACCESS_TOKEN_SECRET

auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

class CustomStreamListener(StreamListener):
    def on_data(self, raw_data):
       print(raw_data)

keyword_list = ["Huawei","apple"]
stream = Stream(auth = api.auth, listener = CustomStreamListener())
stream.filter(track = keyword_list)
